package logic.map;

import repast.simphony.context.Context;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.space.graph.Network;

/**
 * Represents a node (point of interest connected to other elements of the map by a road).
 */
public class Node {
	
	private String id;
	private double posX;
	private double posY;
	private NodeType nodeType;
	
	//Repast interface attributes
	private ContinuousSpace<Object> space;
	private Network network;
	
	public Node(String id, double posX, double posY, NodeType nodeType, ContinuousSpace<Object> space, Context<Object> context) {
		this.setId(id);
		this.setPosX(posX);
		this.setPosY(posY);
		this.setNodeType(nodeType);
		this.setSpace(space);
		
		context.add(this);
		getSpace().moveTo(this, posX, posY);		
	}
	
	public Node(String id, double posX, double posY, NodeType nodeType) {
		this.setId(id);
		this.setPosX(posX);
		this.setPosY(posY);
		this.setNodeType(nodeType);		
	}
	
	public Node(String id, double posX, double posY) {
		this.setId(id);
		this.setPosX(posX);
		this.setPosY(posY);
		this.setNodeType(NodeType.SimpleNode);
	}
	
	public void addToContext(ContinuousSpace<Object> space, Context<Object> context) {
		this.setSpace(space);
		context.add(this);
		getSpace().moveTo(this, posX, posY);
	}
	
	public double getDistanceToNode(Node n) {
		return space.getDistance(new NdPoint(posX, posY), new NdPoint(n.getPosX(), n.getPosY()));
	}
	
	public double getPosX() {
		return posX;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosY() {
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

	public NodeType getNodeType() {
		return nodeType;
	}

	public void setNodeType(NodeType nodeType) {
		this.nodeType = nodeType;
	}

	public ContinuousSpace<Object> getSpace() {
		return space;
	}

	public void setSpace(ContinuousSpace<Object> space) {
		this.space = space;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Network getNetwork() {
		return network;
	}

	public void setNetwork(Network network) {
		this.network = network;
	}
}
