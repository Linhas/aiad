package logic.map;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import repast.simphony.space.graph.Network;

public class GraphUtils {

	public static Network network;
	
	public static Node getMinimumDistance(ArrayList<Node> nodes, HashMap<Node, Double> distances) {
		Node bestNode = null;
		
		for(Node node : nodes) {
			if(bestNode == null) {
				bestNode = node;
			}
			else {
				Double distNode = distances.containsKey(node)? distances.get(node) : Integer.MAX_VALUE;
				Double distBNode = distances.containsKey(bestNode)? distances.get(bestNode) : Integer.MAX_VALUE;
				
				if(distNode < distBNode) {
					bestNode = node;
				}					
			}				
		}
		
		return bestNode;
	}
	
	public static void evaluateSuccessors(Node node, HashMap<Node, Double> distances, HashMap<Node, Node> path, ArrayList<Node> unchecked, ArrayList<Node> checked) {
		//System.out.println("Checking successors for "+node.getId());
		Iterator itr = network.getSuccessors(node).iterator();
		
		while(itr.hasNext()) {
			Node nextNode = (Node)itr.next();
			if(!checked.contains(nextNode)){
				double currentDistance = distances.containsKey(nextNode)? distances.get(nextNode) : Integer.MAX_VALUE;
				double distanceToNode = distances.containsKey(node)? distances.get(node) : Integer.MAX_VALUE;
				double newDistance = distanceToNode + node.getDistanceToNode(nextNode);
				if(currentDistance > newDistance) {
					 distances.put(nextNode, newDistance);
					 path.put(nextNode, node);
					 unchecked.add(nextNode);
				}
			}		 
	    }
	}
	
}
