package logic.map;

import repast.simphony.context.Context;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.graph.Network;

public class GasStation extends Node {
	
	public GasStation(String id, double posX, double posY, ContinuousSpace<Object> space, Context<Object> context) {
		super(id, posX, posY, NodeType.GasStation, space, context);
	}
	
	public GasStation(String id, double posX, double posY) {
		super(id, posX, posY, NodeType.GasStation);
	}
}
