package logic.map;

public enum NodeType {
	TaxiStop,
	GasStation,
	SimpleNode
}
