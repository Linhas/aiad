package logic.map;

import repast.simphony.context.Context;
import repast.simphony.space.continuous.ContinuousSpace;

public class TaxiStop extends Node {
	
	private int taxiNumber;

	//statistics
	private int totalPassengersTransported;
	
	public TaxiStop(String id, double posX, double posY, ContinuousSpace<Object> space, Context<Object> context) {
		super(id, posX, posY, NodeType.TaxiStop, space, context);
		setTotalPassengersTransported(0);
	}
	
	public TaxiStop(String id, double posX, double posY) {
		super(id, posX, posY, NodeType.TaxiStop);
		setTotalPassengersTransported(0);
	}

	public int getTaxiNumber() {
		return taxiNumber;
	}

	public void setTaxiNumber(int taxiNumber) {
		this.taxiNumber = taxiNumber;
	}
	
	public void increaseTaxiNumber() {
		taxiNumber++;
	}

	public void decreaseTaxiNumber() {
		taxiNumber--;
		
		if(taxiNumber < 0)
			taxiNumber = 0;
	}

	public int getTotalPassengersTransported() {
		return totalPassengersTransported;
	}

	public void setTotalPassengersTransported(int totalPassengersTransported) {
		this.totalPassengersTransported = totalPassengersTransported;
	}
	
	public void addTaxiStopPassengersTransported(int number) {
		totalPassengersTransported += number;
	}
}
