package logic.map;

import java.io.File;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

/**
 * Responsible for loading maps from a .map file with a certain structure, in order
 * to get the map elements.
 */
public class MapLoader {
	
	private String fileName; 
	private NodeList roads;
	private HashMap<String, Node> nodes;
	
	public MapLoader(String fileName) {
		this.fileName = fileName;
	}
	
	/**
	 * Loads the contents of the file into a structure that can be used in the program.
	 * Saves the roads in a NodeList and the nodes in a HashMap (with the key being the node's ID and the
	 * value the Node itself - this Node can be a simple node, a gas station or a taxi stop).
	 * 
	 * @return true if the load is successful, false otherwise.
	 */
	public boolean load() {
		try {
			File fXmlFile = new File(fileName);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			doc.getDocumentElement().normalize();
			
			NodeList rootNodes = doc.getElementsByTagName("Node");
			setRoads(doc.getElementsByTagName("Road"));
			setNodes(getNodes(rootNodes));
			
			return true;
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
	}
	
	/**
	 * From a NodeList of nodes, create and add node objects to the HashMap, interpreting the
	 * attributes contained in the .map file.
	 * 
	 * @param nodes
	 * @return the HashMap with the Nodes and their IDs as keys.
	 */
	public HashMap<String, Node> getNodes(NodeList nodes) {
		HashMap<String, Node> nodesHash = new HashMap<String, Node>();
		
		for(int i = 0; i < nodes.getLength(); i++) {
			String id = ((Element)nodes.item(i)).getAttribute("id");
			double x = Double.parseDouble(((Element)nodes.item(i)).getElementsByTagName("startX").item(0).getTextContent());
			double y = Double.parseDouble(((Element)nodes.item(i)).getElementsByTagName("startY").item(0).getTextContent());
			String type = ((Element)nodes.item(i)).getElementsByTagName("nodeType").item(0).getTextContent();
			
			if(type.equals("SimpleNode")) {
				Node simpleNode = new Node(id, x, y);
				nodesHash.put(id, simpleNode);
			}
			else if(type.equals("GasStation")) {
				GasStation simpleNode = new GasStation(id, x, y);
				nodesHash.put(id, simpleNode);
			}
			else if(type.equals("TaxiStop")) {
				int taxiNumber = Integer.parseInt(((Element)nodes.item(i)).getElementsByTagName("taxiNumber").item(0).getTextContent());
				TaxiStop simpleNode = new TaxiStop(id, x, y);
				simpleNode.setTaxiNumber(taxiNumber);
				nodesHash.put(id, simpleNode);
			}
		}
		
		return nodesHash;
	}

	public NodeList getRoads() {
		return roads;
	}

	public void setRoads(NodeList roads) {
		this.roads = roads;
	}

	public HashMap<String, Node> getNodes() {
		return nodes;
	}

	public void setNodes(HashMap<String, Node> nodes) {
		this.nodes = nodes;
	}
			
}
