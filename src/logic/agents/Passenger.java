package logic.agents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import logic.behaviours.PassengerBehaviour;
import logic.map.Node;
import logic.map.NodeType;
import logic.map.TaxiStop;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;

/**
 * Represents a Passenger agent.
 */
public class Passenger extends MovingAgent {

	private NdPoint origin;	
	private Node destination;
	private PassengerState currentState;	
	private TaxiStop closestTaxiStop;
	private long startTime;
	private double waitTime;
	
	public Passenger(ContinuousSpace < Object > space) {
		setSpace(space);
		setCurrentState(PassengerState.WALKING);
	}
	
	/**
	 * Chooses a random starting location for the passenger.
	 */
	public void placeInRandomLocation() {
		//random position
		Random rd = new Random();
		int x = rd.nextInt((int) getSpace().getDimensions().getWidth()); //35
		int y = rd.nextInt((int) getSpace().getDimensions().getHeight()); //40;
		int x2 = rd.nextInt((int) getSpace().getDimensions().getWidth());
		int y2 = rd.nextInt((int) getSpace().getDimensions().getHeight());	
		
		setOrigin(new NdPoint(x,y));
		getSpace().moveTo(this, x, y);	
	}
	
	/**
	 * Chooses a random destination location for the passenger.
	 * @throws Exception 
	 */
	public void chooseRandomDestination(HashMap<String, Node> mapElements) throws Exception {
		if(mapElements.isEmpty())
			throw new Exception("Map is empty. Couldn't choose destination.");
		
		Random rd = new Random();
		Object[] entries = mapElements.values().toArray();
		int x = 0;
		boolean found = false;
		System.out.println(this.getLocalName()+" choosing random destination...");
		int tries = 0;
		while(!found) {
			x = rd.nextInt(entries.length);	
			if(((Node)entries[x]).getNodeType() != NodeType.TaxiStop) {
				setDestination((Node)entries[x]);
				found = true;
				System.out.println("Chosen: "+((Node)entries[x]).getId());
			}
			
			if(tries > 1000) 
				throw new Exception("Couldn't choose destination.");
			else 
				tries++;
		}
		
	}
	
	/**
	 * Checks the map to find the closest taxi stop to the agent.
	 */
	public void chooseTaxiStop(ArrayList<TaxiStop> taxiStops) {
		double bestDistance = Integer.MAX_VALUE;
		double distance1;
		
		for(TaxiStop stop : taxiStops) {			
			System.out.println("\t Taxi Stop: ");
			System.out.println("\t\t Taxi Stop Location: "+getSpace().getLocation(stop));
			System.out.println("\t\t Passenger Location: "+getSpace().getLocation(this));
			distance1 = getSpace().getDistance(new NdPoint(stop.getPosX(), stop.getPosY()), new NdPoint(origin.getX(), origin.getY()));
			System.out.println("\t\t Distance: "+distance1);
			if(distance1 < bestDistance ) {
				bestDistance = distance1;
				setClosestTaxiStop(stop);
				System.out.println("\t\t"+closestTaxiStop.getPosX());
			}
		}
	}

	protected void setup() {		
		System.out.println("Passenger created: "+this.getLocalName());
		PassengerBehaviour tp = new PassengerBehaviour(this, 1000);
		addBehaviour(tp);		
	}
	
	protected void takeDown() {
		System.out.println("Passenger - Take down");
	}
	
	public PassengerState getCurrentState() {
		return currentState;
	}

	public void setCurrentState(PassengerState currentState) {
		this.currentState = currentState;
	}

	public TaxiStop getClosestTaxiStop() {
		return closestTaxiStop;
	}

	public void setClosestTaxiStop(TaxiStop closestTaxiStop) {
		this.closestTaxiStop = closestTaxiStop;
	}

	public Node getDestination() {
		return destination;
	}

	public void setDestination(Node destination) {
		this.destination = destination;
	}

	public NdPoint getOrigin() {
		return origin;
	}

	public void setOrigin(NdPoint origin) {
		this.origin = origin;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public double getWaitTime() {
		return waitTime;
	}

	public void setWaitTime(double waitTime) {
		this.waitTime = waitTime;
	}
	
	public void startWaiting() {
		setStartTime(System.nanoTime());
	}
	
	public void calculateWaitingTime() {
		long elapsed = System.nanoTime() - startTime;
		waitTime = TimeUnit.SECONDS.convert(elapsed, TimeUnit.NANOSECONDS);	
	}

}
