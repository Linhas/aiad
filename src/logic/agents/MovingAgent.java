package logic.agents;

import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

import logic.map.GraphUtils;
import logic.map.Node;
import repast.simphony.space.SpatialMath;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import sajas.core.Agent;
import sajas.domain.DFService;

/**
 * Represents an agent that can move in a space
 * and grid. Contains utility classes for the movement
 * of all agents that extend this class.
 *
 */
public class MovingAgent extends Agent {
	
	//Repast interface attributes
	private ContinuousSpace<Object> space;
	
	/**
	 * Checks if the agent is close to a point.
	 * 
	 * @param dest - destination point
	 * @return true if it's close the destination point, false otherwise
	 */
	public boolean isNearPoint(NdPoint dest) {
		double dist = space.getDistance(getSpace().getLocation(this), dest);
		
		return (dist < 1);
	}
	
	public boolean isNearNode(Node dest) {
		double dist = space.getDistance(getSpace().getLocation(this), new NdPoint(dest.getPosX(), dest.getPosY()));
		
		return (dist < 0.8);
	}
	
	public double getDistanceToNode(Node dest) {
		double dist = space.getDistance(getSpace().getLocation(this), new NdPoint(dest.getPosX(), dest.getPosY()));
		
		return dist;
	}
	
	public boolean isCloseToAgent(MovingAgent a) {		
		NdPoint location = getSpace().getLocation(a);
		
		if(location == null)
			return false;
		
		double dist = space.getDistance(getSpace().getLocation(this), location);
		
		return (dist < 0.8);
	}
	
	/**
	 * Moves the agent towards a point.
	 * 
	 * @param dest - destination point
	 */
	public void moveTowards(NdPoint dest) {
		double angle = SpatialMath.calcAngleFor2DMovement(space, space.getLocation(this), dest);
		space.moveByVector(this, 1, angle, 0);
	}
	
	public void moveTowardsNode(Node node) {
		double angle = SpatialMath.calcAngleFor2DMovement(space, space.getLocation(this), new NdPoint(node.getPosX(), node.getPosY()));
		space.moveByVector(this, 1, angle, 0);		
	}
	
	public void moveToNode(Node node) {
		space.moveTo(this, node.getPosX(), node.getPosY());	
	}
	
	public void hide() {
		space.moveTo(this, 0, 0);	
	}
	
	public ArrayList<Node> findPathTo(Node from, Node to) {
		ArrayList<Node> finalPath = new ArrayList<Node>();
		
		ArrayList<Node> checked = new ArrayList<Node>();
		ArrayList<Node> unchecked = new ArrayList<Node>();
		HashMap<Node, Double> distances = new HashMap<Node, Double>();
		HashMap<Node, Node> path = new HashMap<Node, Node>();
		
		unchecked.add(from);
		distances.put(from, 0.0);
		
		while(unchecked.size() > 0) {
			Node currentMinNode = GraphUtils.getMinimumDistance(unchecked, distances);
			checked.add(currentMinNode);
			unchecked.remove(currentMinNode);
			GraphUtils.evaluateSuccessors(currentMinNode, distances, path, unchecked, checked);
		}
		
		Node step = to;
		finalPath.add(step);
		if(!path.containsKey(step)) {
			return null;
		}
		else {
			while(path.containsKey(step)) {
				step = path.get(step);
				finalPath.add(step);
			}
		}
		
		Collections.reverse(finalPath);
		return finalPath;
	}

	public double calculateDistanceToNode(Node from, Node to) {
		double distance = 0;

		ArrayList<Node> path = findPathTo(from, to);
		
		if(path == null)//already on that node
			return 0;
		
		for(int i = 0; i < path.size(); i ++) {
			if(i < path.size() - 1) {
				NdPoint node1 = new NdPoint(path.get(i).getPosX(), path.get(i).getPosY());
				NdPoint node2 = new NdPoint(path.get(i + 1).getPosX(), path.get(i + 1).getPosY());
				distance += space.getDistance(node1, node2);
			}
		}
		
		return distance;
	}
	
	public ContinuousSpace<Object> getSpace() {
		return space;
	}

	public void setSpace(ContinuousSpace<Object> space) {
		this.space = space;
	}
	
	public DFAgentDescription[] getTaxiServices() {
		DFAgentDescription[] result = null;
		DFAgentDescription dfd = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("transport");
		dfd.addServices(sd);
		try {
			result = DFService.search(this, dfd);
		} catch (FIPAException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	public DFAgentDescription getTaxiManagerService() {
		DFAgentDescription[] result = null;
		DFAgentDescription dfd = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("manager");
		dfd.addServices(sd);
		try {
			result = DFService.search(this, dfd);
		} catch (FIPAException e) {
			e.printStackTrace();
		}
		
		if(result.length < 1)
			return null;
		
		return result[0];
	}
	
	public void sendMessageToAgent(jade.core.AID aid, String message, int messageType) {
		ACLMessage msg = new ACLMessage(messageType);		
		msg.addReceiver(aid);
		msg.setLanguage("PT");
		msg.setContent(message);
		this.send(msg);
	}
	
}
