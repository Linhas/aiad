package logic.agents;

public enum ManagerState {
	STAND_BY, /*waiting for passenger to request service */
	REQUESTING, /*sending request to taxis */
	CHOOSING, /*choosing the best taxi */
	SENDING, /*sending the taxi to collect the passenger */
	WAITING_REPLY /*waiting for taxis to reply */
}
