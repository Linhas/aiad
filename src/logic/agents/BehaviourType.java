package logic.agents;

public enum BehaviourType {
	SIMPLE,
	CLOSEST_STOP,
	CHOOSE_BEST_TAXI
}
