package logic.agents;

public enum PassengerState {
	WAITING, /* Waiting for a taxi to pick him up */
	WALKING, /* Walking to taxi stop */
	REQUEST_TAXI, /* Requesting a taxi */
	REQUEST_TAXI_GLOBAL, /* Requesting a taxi when there is none at the taxi stop */
	ON_THE_WAY, /* Being transported */
	ARRIVED /* Arrived at destination */
}
