package logic.agents;

public enum TaxiState {
	STAND_BY, /* On stand by, at the taxi stop */
	TRANSPORT, /* On transport */
	GOING_HOME, /* Going back to the taxi stop */
	ROLLING, /* Transporting */
	REFUEL, /* Going to refuel */
	WAITING_MANAGER_REPLY /* Waiting for manager response */
}
