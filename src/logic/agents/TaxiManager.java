package logic.agents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Stack;

import org.jfree.util.WaitingImageObserver;

import jade.core.AID;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import jade.lang.acl.ACLMessage;
import logic.behaviours.ManagerBehaviour;
import sajas.core.Agent;
import sajas.domain.DFService;

public class TaxiManager extends Agent {
	
	private ManagerState currentState;
	
	private AID passengerAID;
	private String passengerTaxiStopID;
	private String passengerdestinationID;
	private int waitForTaxis = 0;
	private int waitedForTaxis = 0;
	private HashMap<AID,Double> taxiDistances;
	private AID bestTaxi;
	private Stack<ManagerRequests> requestsQueue;
	
	protected void setup() {
		setTaxiDistances(new HashMap<AID,Double>());
		setCurrentState(ManagerState.STAND_BY);
		setRequestsQueue(new Stack<ManagerRequests>());
		//behaviour
		ManagerBehaviour mb = new ManagerBehaviour(this);
		addBehaviour(mb);
		
		//service
		ServiceDescription service  = new ServiceDescription();
		service.setType("manager");
		service.setName(getLocalName());
        registerService(service);
	}
	
	protected void takeDown() {
		try { 
			DFService.deregister(this); 
		}
	    catch (Exception e) {
	    	
	    }
	}
	
	public void registerService(ServiceDescription sd) {
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
        dfd.addServices(sd);
        
        try {
			DFService.register(this, dfd);
		} catch (FIPAException e) {
			e.printStackTrace();
		} 
	}

	public ManagerState getCurrentState() {
		return currentState;
	}

	public void setCurrentState(ManagerState currentState) {
		this.currentState = currentState;
	}
	
	public void answerRequests() {
		if(requestsQueue.isEmpty()) {
			waitForRequests();
		}
		else {
			ManagerRequests request = requestsQueue.pop();
			answerRequest(request);
		}
	}
	
	public void answerRequest(ManagerRequests request) {
		setPassengerAID(request.getSender());
		setPassengerTaxiStopID(request.getTaxiStopID());
		setPassengerdestinationID(request.getDestinationID());
		String message = "transport,requested";
		ACLMessage msgS = new ACLMessage(ACLMessage.REQUEST);		
		msgS.addReceiver(request.getSender());
		msgS.setLanguage("PT");
		msgS.setContent(message);
		send(msgS);
		setCurrentState(ManagerState.REQUESTING);
	}
	
	public void waitForRequests() {
		ACLMessage msg = receive();
		//while (receive() != null) { }
		if(msg != null) {
			String[] content = msg.getContent().split(",");
			if(content.length == 3 && content[0].equals("transport")) {
				setPassengerAID(msg.getSender());
				setPassengerTaxiStopID(content[1]);
				setPassengerdestinationID(content[2]);
				String message = "transport,requested";
				ACLMessage msgS = new ACLMessage(ACLMessage.REQUEST);		
				msgS.addReceiver(msg.getSender());
				msgS.setLanguage("PT");
				msgS.setContent(message);
				send(msgS);
				setCurrentState(ManagerState.REQUESTING);
			}
		}			
	}
	
	public void sendMessageToTaxis() {
		String message = "location,"+passengerTaxiStopID;
		DFAgentDescription[] result = null;
		DFAgentDescription dfd = new DFAgentDescription();
		ServiceDescription sd = new ServiceDescription();
		sd.setType("transport");
		dfd.addServices(sd);
		try {
			result = DFService.search(this, dfd);
		} catch (FIPAException e) {
			e.printStackTrace();
		}
		
		for(int i = 0; i < result.length; i++) {
			ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);		
			msg.addReceiver(result[i].getName());
			msg.setLanguage("PT");
			msg.setContent(message);
			send(msg);
			waitForTaxis++;
		}
		
		setCurrentState(ManagerState.WAITING_REPLY);
	}
	
	public void waitForTaxisReply() {
		ACLMessage msg = receive();
		
		if(msg != null) {
			String[] content = msg.getContent().split(",");
			if(content.length == 2 && content[0].equals("location")) {
				waitedForTaxis++;
				if(!content[1].equals("unavailable"))
					taxiDistances.put(msg.getSender(), Double.parseDouble(content[1]));
			}
			else if(content.length == 3 && content[0].equals("transport")) {
				ManagerRequests request = new ManagerRequests(msg.getSender(), content[1], content[2]);
				requestsQueue.push(request);
			}
		}
		
		if(waitedForTaxis >= waitForTaxis) {
			waitedForTaxis = 0;
			waitForTaxis = 0;
			setCurrentState(ManagerState.CHOOSING);
		}
	}
	
	public void chooseBestTaxi() {		
		
		if(taxiDistances.isEmpty()) {
			String message = "transport,rejected";
			ACLMessage msg = new ACLMessage(ACLMessage.INFORM);		
			msg.addReceiver(passengerAID);
			msg.setLanguage("PT");
			msg.setContent(message);
			send(msg);
			setCurrentState(ManagerState.STAND_BY);
			return;
		}
		
		double bestDistance = -1;
		
		for(Entry<AID,Double> entry : taxiDistances.entrySet()) {
			if(bestDistance == -1 || entry.getValue() < bestDistance) {
				bestDistance = entry.getValue();
				bestTaxi = entry.getKey();
			}
		}
	
		setCurrentState(ManagerState.SENDING);
	}
	
	public void sendTaxiToPassenger() {
		//String message = "pickup,"+passengerAID.getLocalName()+","+passengerTaxiStopID+","+passengerdestinationID;
		String message = "pickup,"+passengerTaxiStopID;
		ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);		
		msg.addReceiver(bestTaxi);
		msg.setLanguage("PT");
		msg.setContent(message);
		send(msg);
		setCurrentState(ManagerState.STAND_BY);
	}
	
	public void rejectOtherTaxis() {
		String message = "pickup,"+"rejected";
		System.out.println("\t Best Taxi chosen: "+bestTaxi.getLocalName());
		for(Entry<AID,Double> entry : taxiDistances.entrySet()) {
			if(!bestTaxi.getLocalName().equals(entry.getKey().getLocalName())) {
				ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);		
				msg.addReceiver(entry.getKey());
				msg.setLanguage("PT");
				msg.setContent(message);
				send(msg);
			}
		}		
		
		taxiDistances.clear();
	}

	public AID getPassengerAID() {
		return passengerAID;
	}

	public void setPassengerAID(AID passengerAID) {
		this.passengerAID = passengerAID;
	}

	public String getPassengerTaxiStopID() {
		return passengerTaxiStopID;
	}

	public void setPassengerTaxiStopID(String passengerTaxiStopID) {
		this.passengerTaxiStopID = passengerTaxiStopID;
	}

	public String getPassengerdestinationID() {
		return passengerdestinationID;
	}

	public void setPassengerdestinationID(String passengerdestinationID) {
		this.passengerdestinationID = passengerdestinationID;
	}

	public HashMap<AID,Double> getTaxiDistances() {
		return taxiDistances;
	}

	public void setTaxiDistances(HashMap<AID, Double> hashMap) {
		this.taxiDistances = hashMap;
	}

	public AID getBestTaxi() {
		return bestTaxi;
	}

	public void setBestTaxi(AID bestTaxi) {
		this.bestTaxi = bestTaxi;
	}

	public Stack<ManagerRequests> getRequestsQueue() {
		return requestsQueue;
	}

	public void setRequestsQueue(Stack<ManagerRequests> requestsQueue) {
		this.requestsQueue = requestsQueue;
	}
}
