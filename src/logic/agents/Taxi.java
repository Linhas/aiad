package logic.agents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import jade.core.AID;
import jade.domain.FIPAException;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import logic.behaviours.TaxiBehaviour;
import logic.behaviours.TaxiBehaviourAlt;
import logic.behaviours.TaxiBehaviourOpt;
import logic.map.GasStation;
import logic.map.Node;
import logic.map.NodeType;
import logic.map.TaxiStop;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.NdPoint;
import repast.simphony.space.grid.Grid;
import sajas.domain.DFService;

/**
 * Represents a Taxi agent.
 */
public class Taxi extends MovingAgent {

	public final int MAX_CAPACITY = 4;
	public final int MAX_GASOLINE = 200;
	public final int DANGER_GASOLINE = 50;
	
	private int currentFuel;
	private int currentCapacity;
	private NdPoint origin;
	private TaxiState currentState;
	private TaxiState previousState;
	private BehaviourType behaviourType;
	private TaxiStop taxiStop;
	private int queueNumber;	
	private ServiceDescription currentService;	
	private HashMap<jade.core.AID, String> currentPassengers;//passenger AID to destination
	
	private HashMap<String, Node> mapElements;
	private ArrayList<Passenger> people;
	private ArrayList<Passenger> extraTransports;
	
	//statistics
	private int steps;
	private int totalSteps;
	private int totalTransports;
	private int totalPassengersTransported;
	private int totalFuelSpent;
	private double totalMoneySpent;
	private double totalMoneyEarned;	
	
	public Taxi(ContinuousSpace < Object > space) {
		setSpace(space);
	}
	
	public Taxi(ContinuousSpace < Object > space , int queueNumber, TaxiStop taxiStop, BehaviourType behaviour) {
		setSpace(space);
		setQueueNumber(queueNumber);
		setTaxiStop(taxiStop);
		setBehaviourType(behaviour);
	}
	
	protected void setup() {
		setCurrentCapacity(0);
		setCurrentFuel(MAX_GASOLINE);
		setTotalPassengersTransported(0);
		setTotalTransports(0);
		setTotalFuelSpent(0);
		setTotalMoneyEarned(0);
		setTotalMoneySpent(0);
		setPeople(new ArrayList<Passenger>());
		setSteps(0);		
		setOrigin(new NdPoint(taxiStop.getPosX(),taxiStop.getPosY()));
		setCurrentPassengersAID(new HashMap<jade.core.AID, String>());
		setCurrentState(TaxiState.STAND_BY);
		System.out.println("Taxi created: "+this.getLocalName());
		
		//behaviour
		if(behaviourType == BehaviourType.SIMPLE) {
			TaxiBehaviour tb = new TaxiBehaviour(this);
			addBehaviour(tb);
		}
		else if(behaviourType == BehaviourType.CHOOSE_BEST_TAXI){
			TaxiBehaviourAlt tb = new TaxiBehaviourAlt(this);
			addBehaviour(tb);
		}
		else {
			TaxiBehaviourOpt tb = new TaxiBehaviourOpt(this);
			addBehaviour(tb);
		}		
		
		//visualization
		getSpace().moveTo(this, taxiStop.getPosX(),taxiStop.getPosY());
		
		//service
		currentService  = new ServiceDescription();
		currentService.setType("transport");
		currentService.setName(getLocalName());
        registerService(currentService);
	}
	
	protected void takeDown() {
		System.out.println("Taxi - Take down");
		try { 
			DFService.deregister(this); 
		}
        catch (Exception e) {
        	
        }
	}
	
	/*public void removeService(ServiceDescription sd) {
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
		dfd.removeServices(sd);
	}*/
	
	public void registerService(ServiceDescription sd) {
		DFAgentDescription dfd = new DFAgentDescription();
		dfd.setName(getAID());
        dfd.addServices(sd);
        
        try {
			DFService.register(this, dfd);
		} catch (FIPAException e) {
			e.printStackTrace();
		} 
	}
	
	public TaxiState getTaxiState() {
		return currentState;
	}

	public TaxiStop getTaxiStop() {
		return taxiStop;
	}

	public void setTaxiStop(TaxiStop taxiStop) {
		this.taxiStop = taxiStop;
	}

	public void decreaseQueueNumber() {
		queueNumber--;
		if(queueNumber < 0) queueNumber = 0;
	}
	
	public int getQueueNumber() {
		return queueNumber;
	}

	public void setQueueNumber(int queueNumber) {
		this.queueNumber = queueNumber;
	}

	public int getCurrentFuel() {
		return currentFuel;
	}

	public void setCurrentFuel(int currentFuel) {
		this.currentFuel = currentFuel;
	}
	
	public void decreaseCurrentFuel() {
		this.currentFuel--;
		increaseTotalFuelSpent();
	}
	
	public void resetCurrentFuel() {
		this.currentFuel = MAX_GASOLINE;
	}

	public int getCurrentCapacity() {
		return currentCapacity;
	}

	public void setCurrentCapacity(int currentCapacity) {
		this.currentCapacity = currentCapacity;
	}

	public NdPoint getOrigin() {
		return origin;
	}

	public void setOrigin(NdPoint origin) {
		this.origin = origin;
	}

	public TaxiState getCurrentState() {
		return currentState;
	}

	public void setCurrentState(TaxiState currentState) {
		this.currentState = currentState;
	}

	public HashMap<String, Node> getMapElements() {
		return mapElements;
	}

	public void setMapElements(HashMap<String, Node> mapElements) {
		this.mapElements = mapElements;
	}

	public HashMap<jade.core.AID, String> getCurrentPassengersAID() {
		return currentPassengers;
	}

	public void setCurrentPassengersAID(HashMap<jade.core.AID, String> currentPassengersAID) {
		this.currentPassengers = currentPassengersAID;
	}
	
	public int getTotalFuelSpent() {
		return totalFuelSpent;
	}

	public void setTotalFuelSpent(int totalFuelSpent) {
		this.totalFuelSpent = totalFuelSpent;
	}
	
	public void addToFuelSpent(int fuelSpent) {
		this.totalFuelSpent += fuelSpent;
	}
	
	public void increaseTotalFuelSpent() {
		this.totalFuelSpent++;
	}
	
	public double getTotalMoneySpent() {
		return totalMoneySpent;
	}

	public void setTotalMoneySpent(double totalMoneySpent) {
		this.totalMoneySpent = totalMoneySpent;
	}
	
	public void addToMoneySpent(int moneySpent) {
		this.totalMoneySpent += moneySpent;
	}
	
	public double getTotalMoneyEarned() {
		return totalMoneyEarned;
	}

	public void setTotalMoneyEarned(double totalMoneyEarned) {
		this.totalMoneyEarned = totalMoneyEarned;
	}
	
	public void addToMoneyEarned(double moneyEarned) {
		this.totalMoneyEarned += moneyEarned;
	}
	
	public double calculateMoneyEarnedOnTransport() {
		double earned = 0;
		
		for(int i = 0; i < this.currentPassengers.size(); i++) {
			earned += 3.0 + (1.5 * steps);
		}
		
		return earned;
	}
	
	public int getSteps() {
		return steps;
	}

	public void setSteps(int steps) {
		this.steps = steps;
	}
	
	public void increaseSteps() {
		this.steps++;
	}
	
	public int getTotalSteps() {
		return totalSteps;
	}

	public void setTotalSteps(int totalSteps) {
		this.totalSteps = totalSteps;
	}
	
	public void addToTotalSteps(int steps) {
		this.totalSteps += steps;
	}

	public void resetSteps() {
		this.steps = 0;
	}
	
	public int getTotalPassengersTransported() {
		return totalPassengersTransported;
	}

	public void setTotalPassengersTransported(int totalPassengersTransported) {
		this.totalPassengersTransported = totalPassengersTransported;
	}
	
	public boolean isOnTransport() {
		return (currentState != TaxiState.STAND_BY);
	}
	
	public void addPassengerToList(jade.core.AID passenger, String destination) {
		setCurrentCapacity(getCurrentCapacity() + 1);
		this.currentPassengers.put(passenger, destination);
	}
	
	public void emptyPassengerList() {		
		updateStatistics();
		setCurrentCapacity(0);
		resetSteps();
		this.currentPassengers.clear();
	}
	
	public void updateStatistics() {
		totalTransports++;
		totalPassengersTransported += this.currentPassengers.size();
		taxiStop.addTaxiStopPassengersTransported(this.currentPassengers.size());
		addToMoneyEarned(calculateMoneyEarnedOnTransport());
		addToTotalSteps(steps);
		
		System.out.println(getLocalName()+" Transported "+totalPassengersTransported+" Earned "+totalMoneyEarned+" For "+totalSteps+ " steps");
		System.out.println(taxiStop.getId()+" Taxi stop Transported "+taxiStop.getTotalPassengersTransported());
	}
	
	public String getPassengerDestination(int index) {
		if(index >= 0 && index < this.currentPassengers.size())
			return (String) this.currentPassengers.values().toArray()[index];
		else
			return null;
	}
	
	public boolean canTransport() {
		return (!this.currentPassengers.isEmpty() && this.queueNumber == 0);
	}
	
	public boolean hasSpace() {
		return currentCapacity <= MAX_CAPACITY;
	}
	
	public void updateTaxiStopQueuePosition() {
		queueNumber = taxiStop.getTaxiNumber() - 1;
	}
	
	public void joinTaxiStop(TaxiStop stop) {
		setTaxiStop(stop);
		taxiStop.increaseTaxiNumber();
	}
	
	public void leaveTaxiStop() {
		taxiStop.decreaseTaxiNumber();
		System.out.println(getLocalName() + " Leaving stop, " + taxiStop.getTaxiNumber() + " remain.");
	}
	
	public ArrayList<Node> chooseClosestRefuelStation(Node closestNode) {
		GasStation chosenGasStation = null;
		double currentDistance = 0;
		double distanceToOtherNode = 0;
		ArrayList<Node> newPath = new ArrayList<Node>();
		
		for (Entry<String, Node> entry : mapElements.entrySet())
		{
			if(entry.getValue().getNodeType() == NodeType.GasStation) {
				if(chosenGasStation == null) {
					chosenGasStation = (GasStation) entry.getValue();
					currentDistance = getDistanceToNode(chosenGasStation);
				}
				else {
					distanceToOtherNode = getDistanceToNode(entry.getValue());
					if(distanceToOtherNode < currentDistance) {
						chosenGasStation = (GasStation) entry.getValue();
						currentDistance = distanceToOtherNode;
					}
				}
			}
		}
		
		if(chosenGasStation == null)
			return null;
		
		newPath = findPathTo(closestNode, chosenGasStation);
		
		return newPath;
	}
	
	public TaxiStop chooseClosestTaxiStation(Node closestNode) {
		TaxiStop chosenTaxiStop = null;
		double currentDistance = 0;
		double distanceToOtherNode = 0;
		
		for (Entry<String, Node> entry : mapElements.entrySet())
		{
			if(entry.getValue().getNodeType() == NodeType.TaxiStop) {
				if(chosenTaxiStop == null) {
					chosenTaxiStop = (TaxiStop) entry.getValue();
					currentDistance = getDistanceToNode(chosenTaxiStop);
				}
				else {
					distanceToOtherNode = getDistanceToNode(entry.getValue());
					if(distanceToOtherNode < currentDistance) {
						chosenTaxiStop = (TaxiStop) entry.getValue();
						currentDistance = distanceToOtherNode;
					}
				}
			}
		}
		
		return chosenTaxiStop;
	}
	
	public BehaviourType getBehaviourType() {
		return behaviourType;
	}

	public void setBehaviourType(BehaviourType behaviourType) {
		this.behaviourType = behaviourType;
	}

	public int getTotalTransports() {
		return totalTransports;
	}

	public void setTotalTransports(int totalTransports) {
		this.totalTransports = totalTransports;
	}

	public ArrayList<Passenger> getPeople() {
		return people;
	}

	public void setPeople(ArrayList<Passenger> people) {
		this.people = people;
	}
	
	public void addToPeople(Passenger passenger) {
		this.people.add(passenger);
	}
	
	public void setExtraTransports(ArrayList<Passenger> t) {
		extraTransports = t;
	}
	
	public void emptyExtraTransports() {
		extraTransports.clear();
	}
	
	public ArrayList<Passenger> getExtraTransports() {
		return extraTransports;
	}

	public TaxiState getPreviousState() {
		return previousState;
	}

	public void setPreviousState(TaxiState previousState) {
		this.previousState = previousState;
	}
}
