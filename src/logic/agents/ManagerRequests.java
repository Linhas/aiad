package logic.agents;

import jade.core.AID;

public class ManagerRequests {
	private AID sender;
	private String taxiStopID;
	private String destinationID;
	
	public ManagerRequests(AID sender, String taxiStopID, String destinationID) {
		this.setSender(sender);
		this.setTaxiStopID(taxiStopID);
		this.setDestinationID(destinationID);
	}

	public AID getSender() {
		return sender;
	}

	public void setSender(AID sender) {
		this.sender = sender;
	}

	public String getTaxiStopID() {
		return taxiStopID;
	}

	public void setTaxiStopID(String taxiStopID) {
		this.taxiStopID = taxiStopID;
	}

	public String getDestinationID() {
		return destinationID;
	}

	public void setDestinationID(String destinationID) {
		this.destinationID = destinationID;
	}
}
