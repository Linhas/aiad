package logic.behaviours;

import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.lang.acl.ACLMessage;
import logic.agents.Passenger;
import logic.agents.PassengerState;
import repast.simphony.space.continuous.NdPoint;
import sajas.core.behaviours.SimpleBehaviour;

public class PassengerBehaviour extends SimpleBehaviour {
	
	private static final long serialVersionUID = 1L;
	private boolean done = false;
	private boolean resendMessage = true;
	private int tries = 0;
			
	public PassengerBehaviour(Passenger a, long timeout) {
		super(a);
	}
	
	@Override
	public void action() {
		run();
	}
	
	//@ScheduledMethod(start = 1, interval = 1, priority = 0)
	protected void run() {
		switch(getPassenger().getCurrentState()) {
			case WAITING:
				waitForTaxiReply();
				break;
			case ON_THE_WAY:
				//done = true;
				waitToArrive();
				break;
			case WALKING:
				moveToTaxiStop();
				break;
			case ARRIVED:
				break;
			case REQUEST_TAXI:
				requestTaxi();
				break;
			default:
				break;
		}
	}
	
	private void waitToArrive() {
		ACLMessage msg = myAgent.receive();
		if(msg != null) {
			String[] content = msg.getContent().split(",");
			if(content.length == 2 && content[0].equals("transport") && content[1].equals("arrived")) {
				System.out.println("Finished my transport ("+this.getAgent().getLocalName()+") to: " + getPassenger().getDestination().getId() + " . Sent by "+msg.getSender());
				getPassenger().moveToNode(getPassenger().getDestination());
				getPassenger().setCurrentState(PassengerState.ARRIVED);
			}
		}
		else {
			block();
		}
			
	}

	/**
	 * Moves agent to the closest taxi stop.
	 */
	public void moveToTaxiStop() {
		NdPoint dest = new NdPoint(getPassenger().getClosestTaxiStop().getPosX(), getPassenger().getClosestTaxiStop().getPosY());//new NdPoint(pt.getX() + 1, pt.getY());
		getPassenger().moveTowards(dest);		
		if(getPassenger().isNearPoint(dest)) {
			getPassenger().startWaiting();
			getPassenger().setCurrentState(PassengerState.REQUEST_TAXI);
		}
		else
			checkForTaxiTransport();
	}
	
	public void checkForTaxiTransport() {
		ACLMessage msg = myAgent.receive();
		if(msg != null) {
			String[] content = msg.getContent().split(",");
			if(content.length == 2 && content[0].equals("transport") && content[1].equals("OK")) {
				System.out.println("Starting my SPECIAL transport ("+this.getAgent().getLocalName()+") to: " + getPassenger().getDestination().getId() + " . Sent by "+msg.getSender());
				getPassenger().setWaitTime(0);
				getPassenger().setCurrentState(PassengerState.ON_THE_WAY);
			}
		}
		
		//empty messages
		while(myAgent.receive() != null) {}
	}
	
	public void requestTaxi() {
		if(getPassenger().getClosestTaxiStop().getTaxiNumber() < 1) {
			if(resendMessage) {
				System.out.println("\t \t Requesting to manager since stop is empty.");
				warnEmptyStop();
			}				
			else {
				System.out.println("\t \t Waiting a bit for manager to reply.");
				getPassenger().setCurrentState(PassengerState.WAITING);
			}				
		}
		else {		
			System.out.println("\t \t Requesting taxi locally.");
			DFAgentDescription[] services = getPassenger().getTaxiServices();
			String message = "transport," +
							getPassenger().getClosestTaxiStop().getId() + "," +  
							getPassenger().getDestination().getId();		
			for(int i = 0; i < services.length; i++) {
				getPassenger().sendMessageToAgent(services[i].getName(), message, ACLMessage.REQUEST);
			}
			
			getPassenger().setCurrentState(PassengerState.WAITING);
		}
	}
	
	public void waitForTaxiReply() {
		ACLMessage msg = myAgent.receive();
		if(msg != null) {
			String[] content = msg.getContent().split(",");
			if(content.length == 2 && content[0].equals("transport") && content[1].equals("OK")) {
				System.out.println("Starting my transport ("+this.getAgent().getLocalName()+") to: " + getPassenger().getDestination().getId() + " . Sent by "+msg.getSender());
				getPassenger().calculateWaitingTime();
				getPassenger().setCurrentState(PassengerState.ON_THE_WAY);
			}
			else if(content.length == 2 && content[0].equals("transport") && content[1].equals("requested")) {
				System.out.println("Manager answered. Waiting for my transport ("+this.getAgent().getLocalName()+") to: " + getPassenger().getDestination().getId() + " . Sent by "+msg.getSender());
				resendMessage = false;
				getPassenger().setCurrentState(PassengerState.WAITING);				
			}
			else if(content.length == 2 && content[0].equals("transport") && content[1].equals("rejected")) {
				System.out.println("Resending a request soon for my transport ("+this.getAgent().getLocalName()+") to: " + getPassenger().getDestination().getId() + " . Sent by "+msg.getSender());
				getPassenger().setCurrentState(PassengerState.WAITING);
				resendMessage = true;
			}
		}
		else  {
			tries++;
			if(tries >= 10) {
				tries = 0;
				getPassenger().setCurrentState(PassengerState.REQUEST_TAXI);
				//getPassenger().setCurrentState(PassengerState.REQUEST_TAXI_GLOBAL);
			}
				
		}
	}
	
	public void warnEmptyStop() {
		DFAgentDescription service = getPassenger().getTaxiManagerService();
		String message = "transport," +
						getPassenger().getClosestTaxiStop().getId() + "," +  
						getPassenger().getDestination().getId();
		
		getPassenger().sendMessageToAgent(service.getName(), message, ACLMessage.REQUEST);		
		getPassenger().setCurrentState(PassengerState.WAITING);
	}
	
	@Override
	public boolean done() {
		return done;
	}
	
	public Passenger getPassenger() {
		return ((Passenger)getAgent());
	}

}
