package logic.behaviours;

import java.util.ArrayList;
import java.util.Map.Entry;

import jade.core.AID;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.lang.acl.ACLMessage;
import logic.agents.Taxi;
import logic.agents.TaxiState;
import logic.map.Node;
import logic.map.TaxiStop;
import sajas.core.behaviours.SimpleBehaviour;

public class TaxiBehaviourOpt extends SimpleBehaviour {
	
	private static final long serialVersionUID = 1L;
	private boolean done = false;
	
	private ArrayList<Node> path = null;
	private Node lastStop = null;
	private int currentNode = 0;
	private double distance = 0;
	
	public TaxiBehaviourOpt(Taxi a) {
		super(a);
	}
	
	@Override
	public void action() {
		switch(getTaxi().getTaxiState()) {		
			case STAND_BY:
				if(getTaxi().getQueueNumber() > 0) {
					checkForTaxiMessages();
				}					
				else {
					checkForTransportRequests();
				}
				break;
			case GOING_HOME:
				checkForMessagesWhileOnTransport();
				goBackToStation();
				break;
			case ROLLING:
				checkForMessagesWhileOnTransport();
				transportPassenger();
				break;
			case REFUEL:
				checkForMessagesWhileOnTransport();
				goToGasStation();
				break;
			case WAITING_MANAGER_REPLY:
				checkForManagerReply();
				break;
			default:
				break;
		}
	}

	@Override
	public boolean done() {
		return done;
	}
	
	public void checkForTaxiMessages() {
		ACLMessage msg = myAgent.receive();
		if(msg != null && getTaxi().getQueueNumber() > 0) {
			String[] content = msg.getContent().split(",");
			if(content.length == 2 && content[0].equals("leaving") && content[1].equals(getTaxi().getTaxiStop().getId())) {
				getTaxi().decreaseQueueNumber();
			}
			else {
				checkForManagerRequests(content, msg);
			}
		}
	}
	
	public void checkForTransportRequests() {
		ACLMessage msg = myAgent.receive();
		while(msg != null) {
			String[] content = msg.getContent().split(",");
			checkForLocalRequests(content, msg);
			checkForManagerRequests(content, msg);			
			msg = myAgent.receive();
		}
		
		if(getTaxi().canTransport()) {
			startTransport();
		}	
	}
	
	public void checkForLocalRequests(String[] content, ACLMessage msg) {
		if(content.length == 3 && content[0].equals("transport") && content[1].equals(getTaxi().getTaxiStop().getId())) {
			//add to passenger list if the list is empty or if the passenger has the same destination as the first one in the list
			if(getTaxi().getCurrentPassengersAID().isEmpty() || getTaxi().getPassengerDestination(0).equals(content[2]) && getTaxi().hasSpace()) {
				getTaxi().addPassengerToList(msg.getSender(), content[2]);
				String message = "transport,OK";
				warnPassenger(message, msg.getSender());					
			}
		}
	}
	
	public void checkForManagerRequests(String[] content, ACLMessage msg) {	
		if(content.length == 2 && content[0].equals("location")) {
			//manager requests agent to calculate distance to destination
			System.out.println("\t "+getTaxi().getLocalName()+" Received manager request..."+msg.getContent());
			if(getTaxi().getQueueNumber() == 0 && !getTaxi().isOnTransport() && getTaxi().getCurrentPassengersAID().isEmpty()) {
				Node destination = getTaxi().getMapElements().get(content[1]);
				distance = getTaxi().calculateDistanceToNode(getTaxi().getTaxiStop(), destination);
				getTaxi().sendMessageToAgent(msg.getSender(), "location,"+distance, ACLMessage.INFORM);
				getTaxi().setCurrentState(TaxiState.WAITING_MANAGER_REPLY);
			}
			else {
				getTaxi().sendMessageToAgent(msg.getSender(), "location,unavailable", ACLMessage.INFORM);
			}			
		}
	}
	
	public void checkForMessagesWhileOnTransport() {
		ACLMessage msg = myAgent.receive();
		while(msg != null) {
			if(msg != null) {
				String[] content = msg.getContent().split(",");
				checkForManagerRequests(content, msg);
			}
			msg = myAgent.receive();
		}		
	}
	
	public void checkForRequests() {
		ACLMessage msg = myAgent.receive();
		while (msg != null) {
			if(getTaxi().getQueueNumber() == 0) {
				String[] content = msg.getContent().split(",");
				if(content.length == 3 && content[0].equals("transport") && content[1].equals(getTaxi().getTaxiStop().getId())) {
					//add to passenger list if the list is empty or if the passenger has the same destination as the first one in the list
					if(getTaxi().getCurrentPassengersAID().isEmpty() || getTaxi().getPassengerDestination(0).equals(content[2]) && getTaxi().hasSpace()) {
						getTaxi().addPassengerToList(msg.getSender(), content[2]);
						String message = "transport,OK";
						warnPassenger(message, msg.getSender());
						break;
					}					
				}
				else if(content.length == 2 && content[0].equals("location") && getTaxi().getCurrentPassengersAID().isEmpty()) {
					//manager requests agent to calculate distance to destination
					Node destination = getTaxi().getMapElements().get(content[1]);
					distance = getTaxi().calculateDistanceToNode(getTaxi().getTaxiStop(), destination);
					getTaxi().sendMessageToAgent(msg.getSender(), "location,"+distance, ACLMessage.INFORM);
					getTaxi().setCurrentState(TaxiState.WAITING_MANAGER_REPLY);
				}
			}			
			msg = myAgent.receive();
		}
		
		if(getTaxi().canTransport()) 
			startTransport();
	}
	
	private void checkForManagerReply() {
		ACLMessage msg = myAgent.receive();
		if(msg != null) {
			String[] content = msg.getContent().split(",");
			if(content.length == 2 && content[0].equals("pickup")) {
				if(!content[1].equals("rejected")) {
					Node destination = getTaxi().getMapElements().get(content[1]);
					System.out.println(this.getAgent().getLocalName()+" manager requested to leave for taxi stop "+destination.getId());
					path = getTaxi().findPathTo(getTaxi().getTaxiStop(), destination);
					getTaxi().setQueueNumber(Integer.MAX_VALUE);
					getTaxi().leaveTaxiStop();
					warnTaxis();	
					getTaxi().setTaxiStop(((TaxiStop)destination));
					getTaxi().setCurrentState(TaxiState.GOING_HOME);
				}
				else {
					getTaxi().setCurrentState(TaxiState.STAND_BY);
				}
			}			
		}
		/*else {
			tries++;
			
			if(tries > 10) {
				tries = 0;
				getTaxi().setCurrentState(TaxiState.STAND_BY);
			}
		}*/
	}
	
	public void warnTaxis() {
		DFAgentDescription[] services = getTaxi().getTaxiServices();
		String message = "leaving," +
						getTaxi().getTaxiStop().getId();
		for(int i = 0; i < services.length; i++) {
			getTaxi().sendMessageToAgent(services[i].getName(), message, ACLMessage.INFORM);
		}
	}
	
	public void warnPassengers(String message) {
		
		for (Entry<AID, String> entry : getTaxi().getCurrentPassengersAID().entrySet())
		{
			getTaxi().sendMessageToAgent(entry.getKey(), message, ACLMessage.INFORM);
		}
		
	}
	
	public void warnPassenger(String message, AID aid) {
		
		getTaxi().sendMessageToAgent(aid, message, ACLMessage.INFORM);

	}
	
	public void startTransport(){//String destinationAID) {
		String destinationAID = getTaxi().getPassengerDestination(0);
		Node destination = getTaxi().getMapElements().get(destinationAID);
		path = getTaxi().findPathTo(getTaxi().getTaxiStop(), destination);
		getTaxi().setQueueNumber(Integer.MAX_VALUE);
		getTaxi().leaveTaxiStop();
		warnTaxis();
		getTaxi().setCurrentState(TaxiState.ROLLING);
	}
	
	public void restartTrip(){
		if(getTaxi().getCurrentPassengersAID().size() == 0) {
			getTaxi().setTaxiStop(getTaxi().chooseClosestTaxiStation(lastStop));
			path = getTaxi().findPathTo(lastStop, getTaxi().getTaxiStop());
			getTaxi().setCurrentState(TaxiState.GOING_HOME);
		}
		else {
			String destinationAID = getTaxi().getPassengerDestination(0);
			Node destination = getTaxi().getMapElements().get(destinationAID);
			getTaxi().setTaxiStop(getTaxi().chooseClosestTaxiStation(lastStop));
			path = getTaxi().findPathTo(lastStop, destination);
			getTaxi().setCurrentState(TaxiState.ROLLING);
		}		
	}
	
	public void startRefuel(){		
		System.out.println(getTaxi().getLocalName() + " Refueling at "+getTaxi().getCurrentFuel()+" "+lastStop.getId());
		path = getTaxi().chooseClosestRefuelStation(lastStop);
		currentNode = 0;
		getTaxi().setCurrentState(TaxiState.REFUEL);
	}
	
	public void transportPassenger() {
		if(path == null || currentNode >= path.size()) {//finished transport
			//fix taxi location to stand on node
			if(path != null) {
				getTaxi().moveToNode(path.get(path.size() - 1));
				lastStop = path.get(path.size() - 1);
			}
			
			//warn passengers
			String message = "transport,arrived";
			warnPassengers(message);
			
			//go back home
			getTaxi().emptyPassengerList();
			currentNode = 0;
			
			/*if(path != null) 
				Collections.reverse(path);
			else*/
			getTaxi().setTaxiStop(getTaxi().chooseClosestTaxiStation(lastStop));
			path = getTaxi().findPathTo(lastStop, getTaxi().getTaxiStop());
			
			getTaxi().setCurrentState(TaxiState.GOING_HOME);			
		}
		else {
			if(getTaxi().isNearNode(path.get(currentNode))) {
				lastStop = path.get(currentNode);
				if(getTaxi().getCurrentFuel() <= getTaxi().DANGER_GASOLINE && currentNode < path.size()) {
					startRefuel();
				}
				else {
					currentNode++;					
				}	
			}
			else {
				getTaxi().moveTowardsNode(path.get(currentNode));
				getTaxi().increaseSteps();
				getTaxi().decreaseCurrentFuel();
			}
			
		}
	}
	
	public void goBackToStation() {
		
		if(path == null || currentNode >= path.size()) {
			if(path != null) getTaxi().moveToNode(path.get(path.size() - 1));
			lastStop = getTaxi().getTaxiStop();
			while (myAgent.receive() != null) {}
			currentNode = 0;			
			getTaxi().joinTaxiStop(getTaxi().getTaxiStop());
			getTaxi().updateTaxiStopQueuePosition();
			getTaxi().setCurrentCapacity(0);
			getTaxi().setCurrentState(TaxiState.STAND_BY);
			System.out.println(myAgent.getLocalName() + " Back to station "+getTaxi().getTaxiStop().getId() + " with queue number "+getTaxi().getQueueNumber());
		}
		else {
			if(getTaxi().isNearNode(path.get(currentNode))) {
				lastStop = path.get(currentNode);
				if(getTaxi().getCurrentFuel() <= getTaxi().DANGER_GASOLINE) {
					startRefuel();
				}
				else {
					currentNode++;					
				}					
			}
			else {
				getTaxi().moveTowardsNode(path.get(currentNode));
				getTaxi().decreaseCurrentFuel();
			}
		}
		
	}
	
	public void goToGasStation() {
		//path == null means the object is at the correct node
		if(path == null) {
			currentNode = 0;
			getTaxi().addToMoneySpent(50);
			getTaxi().resetCurrentFuel();
			System.out.println(myAgent.getLocalName() + " refueled.");
			restartTrip();
		}
		else if(currentNode >= path.size()) {
			getTaxi().moveToNode(path.get(path.size() - 1));
			currentNode = 0;
			lastStop = path.get(path.size() - 1);
			getTaxi().addToMoneySpent(50);
			getTaxi().resetCurrentFuel();
			System.out.println(myAgent.getLocalName() + " refueled.");
			restartTrip();			
		}
		else {
			if(getTaxi().isNearNode(path.get(currentNode))) {
				lastStop = path.get(currentNode);
				currentNode++;
			}
			else {
				getTaxi().moveTowardsNode(path.get(currentNode));
				getTaxi().decreaseCurrentFuel();
			}
		}
	}
	
	public Taxi getTaxi() {
		return ((Taxi)getAgent());
	}

}
