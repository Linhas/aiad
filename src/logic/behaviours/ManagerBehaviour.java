package logic.behaviours;

import logic.agents.TaxiManager;
import sajas.core.behaviours.SimpleBehaviour;

public class ManagerBehaviour extends SimpleBehaviour {

	private static final long serialVersionUID = 1L;
	private boolean done = false;
	
	public ManagerBehaviour(TaxiManager manager) {
		super(manager);
	}
	
	@Override
	public void action() {
		switch(getManager().getCurrentState()) {
			case STAND_BY:
				getManager().waitForRequests();
				break;
			case REQUESTING:
				getManager().sendMessageToTaxis();
				break;
			case WAITING_REPLY:
				getManager().waitForTaxisReply();
				break;
			case CHOOSING:
				getManager().chooseBestTaxi();
				break;
			case SENDING:
				getManager().rejectOtherTaxis();
				getManager().sendTaxiToPassenger();
				break;
			default:
				break;
		}
	}

	@Override
	public boolean done() {
		return done;
	}
	
	public TaxiManager getManager() {
		return ((TaxiManager)getAgent());
	}

}
