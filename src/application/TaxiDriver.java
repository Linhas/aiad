package application;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.StaleProxyException;
import logic.agents.BehaviourType;
import logic.agents.Passenger;
import logic.agents.Taxi;
import logic.agents.TaxiManager;
import logic.map.GraphUtils;
import logic.map.MapLoader;
import logic.map.Node;
import logic.map.NodeType;
import logic.map.TaxiStop;
import repast.simphony.context.Context;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.graph.Network;
import repast.simphony.space.graph.RepastEdge;
import sajas.core.Runtime;
import sajas.wrapper.AgentController;
import sajas.wrapper.ContainerController;

/**
 * Initializer class for the map and the agents.
 * Contains the necessary functions to start Jade and add agents to containers and contexts
 * and place them in the space. Also makes the connections between the nodes, adding them to
 * the space and the network.
 *
 */
public class TaxiDriver {
	
	//Jade attributes
	ArrayList<AgentController> agents;
	ContainerController agentContainer;
	
	//Repast interface attributes
	public Context<Object> context;
	ContinuousSpace<Object> space;
	Network network;
	
	//loaded map attributes
	ArrayList<TaxiStop> taxiStops;
	int behaviours;
	
	public HashMap<String, Node> mapElements;//HashMap with NodeID as the key and Node as the value
	public ArrayList<Taxi> taxis = new ArrayList<Taxi>();
	public ArrayList<Passenger> passengers = new ArrayList<Passenger>();
	
	public TaxiDriver(Context<Object> context, ContinuousSpace<Object> space, Network network) {
		this.context = context;
		this.space = space;
		this.network = network;
	}
	
	/**
	 * Used to start Jade's main container
	 */
	public void launchJade() {
		Runtime rt = Runtime.instance();
		Profile p1 = new ProfileImpl();
		agentContainer = rt.createMainContainer(p1);
	}
	
	/**
	 * Loads the map from a file and creates the necessary map objects (adding them to the context, 
	 * to the space and connecting them in the network projection) and agents (placing them in their 
	 * initial positions in the space).
	 * 
	 * Saves the map elements in mapElements and the taxi stops in taxiStops.
	 * 
	 * @return true if the map is built with success, false otherwise
	 */
	public boolean buildMap(String mapPath) {
		MapLoader mapLoader = new MapLoader(mapPath);
		if(mapLoader.load()) {
			
			taxiStops = new ArrayList<TaxiStop>();
			mapElements = mapLoader.getNodes();
			
			for(Entry<String, Node> entry : mapElements.entrySet()) {
			    String key = entry.getKey();
			    Node value = entry.getValue();
			    value.addToContext(space, context);
			    if(value.getNodeType() == NodeType.TaxiStop) {
			    	taxiStops.add((TaxiStop)value);
			    	createTaxis(key, ((TaxiStop)value).getTaxiNumber(), (TaxiStop)value);
			    }			    	
			}			
			createEdges(mapLoader.getRoads(), mapLoader.getNodes());
			GraphUtils.network = network;
			return true;
		}
		else {
			return false;
		}
	}
	
	/**
	 * Generates a number of taxis (according to parameter num) associated to a taxi stop.
	 * 
	 * @param id - Taxi Stop's id
	 * @param num - Number of taxis to generate
	 * @param taxiStop - Taxi Stop where this taxi belongs to
	 */
	public void createTaxis(String id, int num, TaxiStop taxiStop) {
		
		for(int i = 0; i < num; i++) {
			Taxi taxi = new Taxi(space, i, taxiStop, getBehaviourType());
			taxi.setMapElements(mapElements);
			taxis.add(taxi);
			context.add(taxi);
			try {
				AgentController ac = agentContainer.acceptNewAgent(id + "_T" + i, taxi);
				ac.start();
			} catch (StaleProxyException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Creates the connections (roads) between map elements (points of interest - gas stations, 
	 * taxi stops, potential passenger destinations).
	 * 
	 * @param edges - List of connections
	 * @param nodes - HashMap with all the nodes
	 */
	public void createEdges(NodeList edges, HashMap<String, Node> nodes) {
		for(int i = 0; i < edges.getLength(); i++) {
			String id1 = ((Element)edges.item(i)).getAttribute("from");
			String id2 = ((Element)edges.item(i)).getAttribute("to");
			Node n1 = nodes.get(id1);
			Node n2 = nodes.get(id2);
			RepastEdge<Node> edge = new RepastEdge<Node>(n1, n2, false, 2);
			network.addEdge(edge);
		}
	}
	
	/**
	 * Generates a number of agent passengers according to the parameter.
	 * 
	 * @param numPassengers - Number of passengers to create.
	 */
	public void createPassengers(int numPassengers) {
		for (int i = 0; i < numPassengers; i++) {
			Passenger passenger = new Passenger(space);
			context.add(passenger);
			passenger.placeInRandomLocation();
			passenger.chooseTaxiStop(taxiStops);
			passengers.add(passenger);
			try {
				AgentController ac = agentContainer.acceptNewAgent("Passenger" + i, passenger);
				passenger.chooseRandomDestination(mapElements);
				ac.start();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void listPassengersToTaxi() {
		for(Taxi taxi : taxis) {
			taxi.setPeople(passengers);
		}
	}
	
	public void startTaxiManager() {
		TaxiManager manager = new TaxiManager();
		try {
			AgentController ac = agentContainer.acceptNewAgent("Manager", manager);
			ac.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void setTaxiBehaviour(int taxiBehaviour) {
		behaviours = taxiBehaviour;		
	}
	
	public BehaviourType getBehaviourType() {
		switch(behaviours) {
			case 1:
				return BehaviourType.SIMPLE;
			case 2:
				return BehaviourType.CLOSEST_STOP;
			case 3: 
				return BehaviourType.CHOOSE_BEST_TAXI;
			case 4:
				Random rnd = new Random();
				int choice = rnd.nextInt(3) + 1;
				if(choice == 1)
					return BehaviourType.SIMPLE;
				else if(choice == 2)
					return BehaviourType.CHOOSE_BEST_TAXI;
				else
					return BehaviourType.CLOSEST_STOP;
			default:
				return BehaviourType.SIMPLE;
		}
	}
}
