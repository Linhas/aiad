package application;

import repast.simphony.context.space.continuous.ContinuousSpaceFactory;
import repast.simphony.context.space.continuous.ContinuousSpaceFactoryFinder;
import repast.simphony.context.space.graph.NetworkBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.parameter.Parameters;
import repast.simphony.space.continuous.ContinuousSpace;
import repast.simphony.space.continuous.RandomCartesianAdder;
import repast.simphony.space.graph.Network;
import sajas.sim.repasts.RepastSLauncher;

public class Main extends RepastSLauncher {
	
	TaxiDriver taxiDriver;
	
	@Override
	public String getName() {
		return "TaxiDriver";
	}

	@Override
	protected void launchJADE() {
		mainContext.setId("TaxiDriver");
		
		NetworkBuilder builder = new NetworkBuilder("network", mainContext, false);
		Network network = builder.buildNetwork();
		
		//create space
		ContinuousSpaceFactory spaceFactory = ContinuousSpaceFactoryFinder
				.createContinuousSpaceFactory(null);
		ContinuousSpace<Object> space = spaceFactory.createContinuousSpace(
				"space", mainContext, new RandomCartesianAdder<Object>(),
				new repast.simphony.space.continuous.StrictBorders(), 50,
				50);
		
		//create agents
		Parameters params = RunEnvironment.getInstance().getParameters();
		int numPassengers = (Integer) params.getValue("numPassengers");
		int taxiBehaviour = (Integer) params.getValue("taxiBehaviour");
		String mapPath = (String) params.getValue("mapPath");
		
		taxiDriver = new TaxiDriver(mainContext, space, network);
		taxiDriver.setTaxiBehaviour(taxiBehaviour);
		taxiDriver.launchJade();
		
		System.out.println("====================================");
		System.out.println("Loading map...");
		if(taxiDriver.buildMap(mapPath)) {
			System.out.println("Map built and loaded.");
			taxiDriver.createPassengers(numPassengers);
			taxiDriver.listPassengersToTaxi();
			taxiDriver.startTaxiManager();
			System.out.println("Agents created.");
		}			
		else {
			System.out.println("Error reading from map file. Exiting.");
			System.exit(1);
		}
		System.out.println("====================================");
			
	}

}
